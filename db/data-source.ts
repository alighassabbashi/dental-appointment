import { DataSource, DataSourceOptions } from "typeorm";

export const dataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'aligh1375??@',
  database: process.env.NODE_ENV === 'development' ? 'dental_appointment':'test_dental_appointment',
  entities: [`dist/**/*.entity.${process.env.NODE_ENV === 'development' ? 'js':'ts'}`],
  migrations: ['dist/db/migrations/*.js'],
}

const dataSource = new DataSource(dataSourceOptions);
export default dataSource;