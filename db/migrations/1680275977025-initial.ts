import { MigrationInterface, QueryRunner } from "typeorm";

export class initial1680275977025 implements MigrationInterface {
    name = 'initial1680275977025'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "unregistered_user" ("_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "lastname" character varying NOT NULL, "insurance_type" character varying NOT NULL, "national_code" character varying NOT NULL, CONSTRAINT "PK_fe8824bfbe6519548a68701f338" PRIMARY KEY ("_id"))`);
        await queryRunner.query(`CREATE TABLE "service" ("_id" SERIAL NOT NULL, "name" character varying NOT NULL, "identification_short_code" integer NOT NULL, "cost" integer NOT NULL, "duration" integer NOT NULL, "is_available" boolean NOT NULL, CONSTRAINT "PK_b99bce2b6999d1872fe69a4e199" PRIMARY KEY ("_id"))`);
        await queryRunner.query(`CREATE TABLE "visit" ("_id" SERIAL NOT NULL, "status" character varying NOT NULL DEFAULT 'initiated', "dateTime" character varying NOT NULL, "duration" integer NOT NULL, "cost" integer NOT NULL, "user_id" uuid, "unregisteredUser_id" uuid, "invoice_id" integer, CONSTRAINT "REL_b0be45ec88747ae883bfb4f9bb" UNIQUE ("invoice_id"), CONSTRAINT "PK_35a9ef3af493162d34c4f33f352" PRIMARY KEY ("_id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "lastname" character varying NOT NULL, "age" integer NOT NULL, "gender" character varying NOT NULL, "insurance_type" character varying NOT NULL, "phone_number" character varying NOT NULL, "email" character varying NOT NULL, "national_code" character varying NOT NULL, "role" character varying NOT NULL DEFAULT 'basic', "password" character varying NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_457bfa3e35350a716846b03102d" PRIMARY KEY ("_id"))`);
        await queryRunner.query(`CREATE TABLE "invoice" ("_id" SERIAL NOT NULL, "status" character varying NOT NULL DEFAULT 'initiated', "cost" integer NOT NULL, "discount" integer NOT NULL, "tracking_code" uuid NOT NULL DEFAULT uuid_generate_v4(), "user_id" uuid, CONSTRAINT "PK_5daf8daca77cd608b70dbde8a5c" PRIMARY KEY ("_id"))`);
        await queryRunner.query(`CREATE TABLE "visit_required_services_service" ("visit_id" integer NOT NULL, "service_id" integer NOT NULL, CONSTRAINT "PK_0d7067f774e4d2b237b89c9771a" PRIMARY KEY ("visit_id", "service_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_ad4cc45afbdc0ff24c72b8635c" ON "visit_required_services_service" ("visit_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_46512630691af16abdcadd1127" ON "visit_required_services_service" ("service_id") `);
        await queryRunner.query(`ALTER TABLE "visit" ADD CONSTRAINT "FK_359bbdf2205ac56732464652f99" FOREIGN KEY ("user_id") REFERENCES "user"("_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "visit" ADD CONSTRAINT "FK_870ed1a9f585d6b8389ab7560a8" FOREIGN KEY ("unregisteredUser_id") REFERENCES "unregistered_user"("_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "visit" ADD CONSTRAINT "FK_b0be45ec88747ae883bfb4f9bb4" FOREIGN KEY ("invoice_id") REFERENCES "invoice"("_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "invoice" ADD CONSTRAINT "FK_c14b00795593eafc9d423e7f74d" FOREIGN KEY ("user_id") REFERENCES "user"("_id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "visit_required_services_service" ADD CONSTRAINT "FK_ad4cc45afbdc0ff24c72b8635ca" FOREIGN KEY ("visit_id") REFERENCES "visit"("_id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "visit_required_services_service" ADD CONSTRAINT "FK_46512630691af16abdcadd1127e" FOREIGN KEY ("service_id") REFERENCES "service"("_id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "visit_required_services_service" DROP CONSTRAINT "FK_46512630691af16abdcadd1127e"`);
        await queryRunner.query(`ALTER TABLE "visit_required_services_service" DROP CONSTRAINT "FK_ad4cc45afbdc0ff24c72b8635ca"`);
        await queryRunner.query(`ALTER TABLE "invoice" DROP CONSTRAINT "FK_c14b00795593eafc9d423e7f74d"`);
        await queryRunner.query(`ALTER TABLE "visit" DROP CONSTRAINT "FK_b0be45ec88747ae883bfb4f9bb4"`);
        await queryRunner.query(`ALTER TABLE "visit" DROP CONSTRAINT "FK_870ed1a9f585d6b8389ab7560a8"`);
        await queryRunner.query(`ALTER TABLE "visit" DROP CONSTRAINT "FK_359bbdf2205ac56732464652f99"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_46512630691af16abdcadd1127"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ad4cc45afbdc0ff24c72b8635c"`);
        await queryRunner.query(`DROP TABLE "visit_required_services_service"`);
        await queryRunner.query(`DROP TABLE "invoice"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "visit"`);
        await queryRunner.query(`DROP TABLE "service"`);
        await queryRunner.query(`DROP TABLE "unregistered_user"`);
    }

}
