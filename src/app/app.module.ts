import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';

import { UserModule } from 'src/user/user.module';
import { UnregisteredUsersModule } from 'src/unregistered-user/unregistered-user.module';
import { VisitModule } from 'src/visit/visit.module';
import { InvoiceModule } from 'src/invoice/invoice.module';
import { ServiceModule } from 'src/service/service.module';

import { dataSourceOptions } from 'db/data-source';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `.env.${process.env.NODE_ENV}`
    }),
    TypeOrmModule.forRoot(dataSourceOptions),
    UserModule,
    UnregisteredUsersModule,
    VisitModule,
    InvoiceModule,
    ServiceModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
