import { IsNumber, IsString, Max, Min } from "class-validator";

import { Visit } from "../../visit/visit.entity";

export class CreateInvoiceDto {
  @IsNumber()
  @Min(0)
  @Max(100)
  discount: number;

  @IsString()
  visit: Visit;
}