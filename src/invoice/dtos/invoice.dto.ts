import { Expose, Transform } from "class-transformer";
import { User } from "src/user/user.entity";

export class InvoiceDto {
  @Expose()
  _id: string;

  @Expose()
  status: string;

  @Expose()
  cost: number;

  @Expose()
  discount: number;

  @Expose()
  tracking_code: number;

  @Transform(({ obj }) => obj.user._id)
  @Expose()
  userId: User;
}