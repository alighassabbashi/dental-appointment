import { IsIn, IsNumber, IsObject, IsOptional, IsString, IsUUID, Max, Min } from 'class-validator';

import { User } from '../../user/user.entity';

export class UpdateInvoiceDto {

  @IsString()
  @IsIn(['awaiting-payment', 'confirmed', 'settled', 'archived'])
  @IsOptional()
  status: string;

  @IsNumber()
  @Min(0)
  @IsOptional()
  cost: number;

  @IsNumber()
  @Min(0)
  @Max(100)
  @IsOptional()
  discount: number;
  
}