import { Controller, Post, Body, Get, Param, UseGuards, Delete, Patch } from '@nestjs/common';

import { AuthGuard } from '../guards/auth.guard';
import { CreateInvoiceDto } from './dtos/create-invoice.dto';
import { InvoiceDto } from './dtos/invoice.dto';
import { InvoiceService } from './invoice.service';
import { Serialize } from '../interceptors/serialize.interceptor';
import { CurrentUser } from '../user/decorators/current-user.decorator';
import { User } from '../user/user.entity';
import { AdminGuard } from '../guards/admin.guard';
import { UpdateInvoiceDto } from './dtos/update-invoice.dto';

@Controller('invoice')
@Serialize(InvoiceDto)
export class InvoiceController {
  constructor(private invoiceService: InvoiceService) { }

  @Post('/create')
  @UseGuards(AuthGuard)
  create(@Body() body: CreateInvoiceDto) {
    return this.invoiceService.create(body);
  }

  @Get('/user-invoices')
  @UseGuards(AuthGuard)
  findUserInvoices(@CurrentUser() user: User) {
    return this.invoiceService.findUserInvoices(user);
  }

  @Get('/:id')
  @UseGuards(AuthGuard)
  findInvoice(@Param('id') id: string, @CurrentUser() currentUser: User) {
    return this.invoiceService.findInvoice(id, currentUser);
  }

  @Get('/')
  @UseGuards(AdminGuard)
  findAllInvoices() {
    return this.invoiceService.findAll();
  }

  @Patch('/:id')
  updateInvoice(@Param('id') id: string, @CurrentUser() currentUser: User, @Body() body: UpdateInvoiceDto) {
    return this.invoiceService.update(id, currentUser, body);
  }

  @Delete('/:id')
  @UseGuards(AdminGuard)
  delete(@Param('id') id: string, @CurrentUser() currentUser: User) {
    return this.invoiceService.delete(id);
  }
}
