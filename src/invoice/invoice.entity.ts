import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, Generated } from 'typeorm'

import { User } from '../user/user.entity';

@Entity()
export class Invoice {
  @PrimaryGeneratedColumn()
  _id: string;

  @Column({default: 'initiated'})
  status: string;

  @Column()
  cost: number;

  @Column()
  discount: number;

  @Generated('uuid')
  @Column()
  tracking_code: number;

  @ManyToOne(() => User, (user) => user.invoices)
  user: User;
}