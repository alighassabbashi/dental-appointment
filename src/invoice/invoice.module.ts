import { Module } from '@nestjs/common';
import { TypeOrmModule }from '@nestjs/typeorm'
import { InvoiceController } from './invoice.controller';
import { InvoiceService } from './invoice.service';
import {Invoice }from './invoice.entity'
import { VisitModule } from '../visit/visit.module';

@Module({
  imports: [TypeOrmModule.forFeature([Invoice]), VisitModule],
  controllers: [InvoiceController],
  providers: [InvoiceService]
})
export class InvoiceModule {}
