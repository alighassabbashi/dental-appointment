import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CreateInvoiceDto } from './dtos/create-invoice.dto';
import { Invoice } from './invoice.entity';
import { VisitService } from '../visit/visit.service';
import { User } from '../user/user.entity';

@Injectable()
export class InvoiceService {
  constructor(private visitService: VisitService, @InjectRepository(Invoice) private repo: Repository<Invoice>) { }

  async create(body: CreateInvoiceDto) {
    const visit = await this.visitService.findById(body.visit.toString());
    const invoice = this.repo.create(body);
    invoice.cost = visit.cost;
    invoice.discount = body.discount;
    invoice.user = visit.user;
    const createdInvoice = await this.repo.save(invoice)
    await this.visitService.update(visit._id, { invoice: createdInvoice });
    return createdInvoice;
  }

  async findById(id: string) {
    const invoice = await this.repo.findOne({
      where: { _id: id }, relations: {
      user: true
    } });
    return invoice;
  }

  async findInvoice(id: string, currentUser: User) {
    const invoice = await this.findById(id);
    if (invoice.user._id !== currentUser._id) {
      if (currentUser.role !== 'admin') {
        throw new UnauthorizedException('you are not allowed to get this invoice!')
      }
    }
    return invoice;
  }

  async findAll() {
    const invoices = await this.repo.find({
      relations: {
        user: true
      }
    })
    return invoices;
  }

  async findUserInvoices(user: User) {
    const invoices = await this.repo.find({
      where: { user }, relations: {
      user: true
      }
    })
    return invoices;
  }

  async update(id: string, currentUser: User, payload: Partial<Invoice>) {
    const invoice = await this.findInvoice(id, currentUser);
    Object.assign(invoice, payload);
    return this.repo.save(invoice);
  }

  async delete(id: string) {
    const invoice = await this.findById(id);
    return this.repo.remove(invoice);
  }
}
