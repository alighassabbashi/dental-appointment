import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common'
import { AppModule } from './app/app.module';
const CookieSession = require('cookie-session')

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(CookieSession({
    keys: ['aligh12345']
  }))
  app.useGlobalPipes(new ValidationPipe(
    {whitelist: true}
  ))
  await app.listen(3000);
  console.log('app is lestening on port 3000');
}
bootstrap();
