import { IsBoolean, IsNumber, IsString } from "class-validator";

export class CreateServiceDto {

  @IsString()
  name: string;

  @IsNumber()
  identification_short_code: number;

  @IsNumber()
  cost: number;

  @IsNumber()
  duration: number;

  @IsBoolean()
  is_available: boolean;
}