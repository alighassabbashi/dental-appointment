import { IsBoolean, IsNumber, IsString, IsOptional } from "class-validator";

export class UpdateServiceDto {

  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  identification_short_code: number;

  @IsNumber()
  @IsOptional()
  cost: number;

  @IsNumber()
  @IsOptional()
  duration: number;

  @IsBoolean()
  @IsOptional()
  is_available: boolean;
}