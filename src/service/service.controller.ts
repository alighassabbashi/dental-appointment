import { Param,Body, Controller, Delete, Get, Patch, Post, UseGuards } from '@nestjs/common';

import { CreateServiceDto } from './dtos/create-service.dto';
import { UpdateServiceDto } from './dtos/update-service.dto';
import { ServiceService } from './service.service';
import { AdminGuard } from '../guards/admin.guard';
import { AuthGuard } from '../guards/auth.guard';

@Controller('service')
export class ServiceController {
  constructor(private serviceService: ServiceService) {}

  @Post('/create')
  @UseGuards(AdminGuard)
  createService(@Body() body: CreateServiceDto) {
    return this.serviceService.create(body);
  }

  @Get('/')
  @UseGuards(AdminGuard)
  getAll() {
    return this.serviceService.findAll();
  }

  @Get('/available')
  @UseGuards(AuthGuard)
  getAvailableServices() {
    return this.serviceService.findAvailable();
  }

  @Get('/:id')
  @UseGuards(AuthGuard)
  getService(@Param('id') id: string) {
   return this.serviceService.findById(id);
  }

  @Patch('/:id')
  @UseGuards(AdminGuard)
  updateService(@Param('id') id: string, @Body() payload: UpdateServiceDto) {
    return this.serviceService.update(id, payload);
  }

  @Delete('/:id')
  @UseGuards(AdminGuard)
  deleteService(@Param('id') id: string) {
    return this.serviceService.delete(id);
  }
}
