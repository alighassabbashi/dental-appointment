import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Service {
  @PrimaryGeneratedColumn()
  _id: string;

  @Column()
  name: string;

  @Column()
  identification_short_code: number;

  @Column()
  cost: number;

  @Column()
  duration: number;

  @Column()
  is_available: boolean;
}