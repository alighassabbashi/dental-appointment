import { Injectable, NotFoundException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Service } from './service.entity';
import { CreateServiceDto } from './dtos/create-service.dto';

@Injectable()
export class ServiceService {
  constructor(@InjectRepository(Service) private repo: Repository<Service>) { }

  create(service: CreateServiceDto) {
    const createdService = this.repo.create(service);
    return this.repo.save(createdService);
  }

  findAll() {
    return this.repo.find();
  }

  findAvailable() {
    return this.repo.find({where: {is_available: true}})
  }

  async findById(id: string) {
    if (!id) {
      return null;
    }
    const service = await this.repo.findOne({ where: { _id: id } });
    if (!service) {
      throw new NotFoundException('no service by given id!');
    }
    return service;
  }

  async update(id: string, payload: Partial<Service>) {
    const service = await this.findById(id);
    if (!service) {
      throw new NotFoundException('no service by given id');
    }
    Object.assign(service, payload);
    return this.repo.save(service);
  }

  async delete(id: string) {
    const service = await this.findById(id);
    if (!service) {
      throw new NotFoundException('no service by given id');
    }
    return this.repo.remove(service);
  }
}
