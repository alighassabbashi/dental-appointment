import { IsString, IsIn, Length } from 'class-validator';

export class CreateUnregisteredUserDto {

  @IsString()
  name: string;

  @IsString()
  lastname: string;

  @IsString()
  @IsIn(['tamin', 'niroo', 'talayi', 'takmili', 'roostayi', 'salamat'])
  insurance_type: string;

  @Length(10,10)
  national_code: string;
}