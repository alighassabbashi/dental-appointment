import { Expose } from 'class-transformer'

export class UnregisteredUserDto {
  @Expose()
  _id: string;

  @Expose()
  name: string;

  @Expose()
  lastname: string;

  @Expose()
  insurance_type: string;

  @Expose()
  national_code: string;
}