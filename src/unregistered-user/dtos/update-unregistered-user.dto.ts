import { IsIn, IsOptional, IsString, Length } from 'class-validator';

export class UpdateUnregisteredUserDto {

  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  lastname: string;

  @IsString()
  @IsOptional()
  @IsIn(['tamin', 'niroo', 'talayi', 'takmili', 'roostayi', 'salamat'])
  insurance_type: string;

  @Length(10, 10)
  @IsOptional()
  national_code: string;
}