import { Test, TestingModule } from '@nestjs/testing';
import { UnregisteredUserController } from './unregistered-user.controller';

describe('UnregisteredUserController', () => {
  let controller: UnregisteredUserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UnregisteredUserController],
    }).compile();

    controller = module.get<UnregisteredUserController>(UnregisteredUserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
