import { Body, Controller, Delete, Get, NotFoundException, Param, Patch, Post, Query } from '@nestjs/common';

import { UnregisteredUserService } from './unregistered-user.service'
import { CreateUnregisteredUserDto } from './dtos/create-unregistered-user.dto'
import { UpdateUnregisteredUserDto } from './dtos/update-unregistered-user.dto'
import { UnregisteredUser } from './unregistered-user.entity'
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { UnregisteredUserDto } from './dtos/unregistered-user.dto';

@Controller('unregistered-user')
@Serialize(UnregisteredUserDto)
export class UnregisteredUserController {
  constructor(private unregisteredUserService: UnregisteredUserService) { }
  
  @Post('/create')
  async createUser(@Body() body: CreateUnregisteredUserDto) {
    const user = await this.unregisteredUserService.create(body);
    return user;
  }

  @Get()
  getAllUsers() {
    return this.unregisteredUserService.findAll()
  }

  @Get('/search')
  searchAmongUsers(@Query() payload: Partial<UnregisteredUser>) {
    return this.unregisteredUserService.findByPartialInfo(payload);
  }

  @Get('/:id')
  async getUserById(@Param('id') id: string) {
    const user = await this.unregisteredUserService.findById(id);
    if (!user) {
      throw new NotFoundException("user not found!");
    }
    return user;
  }

  @Patch('/:id')
  updateUser(@Param('id') id: string, @Body() payload: UpdateUnregisteredUserDto) {
    return this.unregisteredUserService.update(id, payload);
  }

  @Delete('/:id')
  deleteUser(@Param('id') id: string) {
    return this.unregisteredUserService.delete(id);
  }
}
