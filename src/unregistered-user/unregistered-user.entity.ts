import { Entity, Column, Generated, PrimaryColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Visit } from '../visit/visit.entity'

@Entity()
export class UnregisteredUser {
  @PrimaryGeneratedColumn('uuid')
  _id: string;

  @Column()
  name: string;

  @Column()
  lastname: string;

  @Column()
  insurance_type: string;

  @OneToMany(() => Visit, (visit) => visit.unregistered_user)
  visits: Visit[];

  @Column()
  national_code: string;
}