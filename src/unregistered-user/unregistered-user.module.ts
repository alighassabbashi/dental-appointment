import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import {UnregisteredUserController } from './unregistered-user.controller';
import { UnregisteredUserService } from './unregistered-user.service';
import { UnregisteredUser } from './unregistered-user.entity'

@Module({
  imports: [TypeOrmModule.forFeature([UnregisteredUser])],
  controllers: [UnregisteredUserController],
  providers: [UnregisteredUserService]
})
export class UnregisteredUsersModule {}
