import { Test, TestingModule } from '@nestjs/testing';
import { UnregisteredUserService } from './unregistered-user.service';

describe('UnregisteredUserService', () => {
  let service: UnregisteredUserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UnregisteredUserService],
    }).compile();

    service = module.get<UnregisteredUserService>(UnregisteredUserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
