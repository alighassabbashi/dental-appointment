import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UnregisteredUser } from './unregistered-user.entity';
import { CreateUnregisteredUserDto } from './dtos/create-unregistered-user.dto'

@Injectable()
export class UnregisteredUserService {
  constructor(@InjectRepository(UnregisteredUser) private repo: Repository<UnregisteredUser>) { }
  
  create(unregisteredUser: CreateUnregisteredUserDto) {
    const createdUser = this.repo.create(unregisteredUser);
    return this.repo.save(createdUser);
  }

  async findById(id: string) {
    if (!id) {
      return null;
    }
    const user = await this.repo.findOne({ where: { _id: id } });
    return user;
  }

  async findAll() {
    const unregisteredUsers = await this.repo.find();
    return unregisteredUsers;
  }

  async findByPartialInfo(payload: Partial<UnregisteredUser>) {
    const users = await this.repo.find({where: payload})
    return users;
  }

  async update(id: string, payload: Partial<UnregisteredUser>) {
    const user = await this.findById(id);
    if (!user) {
      throw new NotFoundException("user not found!");
    }
    Object.assign(user, payload);
    return this.repo.save(user);
  }

  async delete(id: string) {
    const user = await this.findById(id);
    if (!user) {
      throw new NotFoundException('user not found');
    }
    return this.repo.delete(id);
  }
}
