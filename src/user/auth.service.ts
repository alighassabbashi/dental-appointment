import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { scrypt as _script, randomBytes, createHmac } from 'crypto'
import { promisify } from 'util';
import { ConfigService } from '@nestjs/config';

import { UserService } from './user.service';
import { CreateUserDto } from './dtos/create-user.dto'

const scrypt = promisify(_script);

@Injectable()
export class AuthService {
  constructor(private userService: UserService, private configService: ConfigService) { }
  
  async signup(body: CreateUserDto) {
    const userArrgs = { ...body };
    const user = await this.userService.findByEmail(userArrgs.email);
    if (user) {
      throw new BadRequestException('email in use');
    }
    const salt = randomBytes(8).toString('hex').slice(0.16);
    const hash = (await scrypt(userArrgs.password, salt, 32)) as Buffer;
    const hashedPassword = `${salt}.${hash.toString('hex')}`;
    userArrgs.password = hashedPassword;

    const createdUser = await this.userService.create(userArrgs);
    return createdUser;

  }

  async signin(email: string, password: string) {
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new NotFoundException('no user with given email');
    }
    const [salt, storedHash] = user.password.split('.');
    const hash = (await scrypt(password, salt, 32)) as Buffer;
    if (storedHash !== hash.toString('hex')) {
      throw new BadRequestException('wrong password');
    }
    return user;
  }

  async changeRole(id: string, adminKey: string, role: string) {
    const adminCheckSalt = this.configService.get<string>('ADMIN_CHECK_SALT');
    console.log(process.env.NODE_ENV);
    
    const hashedAdminKey = ((await scrypt(adminKey, adminCheckSalt, 32)) as Buffer).toString('hex');
    const storedAdminKey = this.configService.get<string>('ADMIN_KEY');
    if (storedAdminKey === hashedAdminKey) {
      const user = await this.userService.updateRole(id, role)
      return `user role changed to ${user.role}`;
    } else {
      throw new UnauthorizedException('you are not allowd to do this action');
    }
  }
}