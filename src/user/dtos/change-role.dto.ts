import { IsIn, IsString } from "class-validator";

export class ChangeRoleDto {
  @IsString()
  @IsIn(['basic', 'admin'])
  role: string;

  @IsString()
  adminKey: string;
}