import { IsString, IsNumber, IsEmail, Min, Max, IsIn, MinLength, ValidateIf, Matches, IsAlphanumeric, Length } from 'class-validator';

export class CreateUserDto {

  @IsString()
  name: string;

  @IsString()
  lastname: string;

  @IsNumber()
  @Min(0)
  @Max(100)
  age: number;

  @IsString()
  @IsIn(['male', 'female'])
  gender: string;

  @IsString()
  @IsIn(['tamin', 'niroo', 'talayi', 'takmili', 'roostayi', 'salamat'])
  insurance_type: string;

  @IsString()
  @MinLength(11)
  phone_number: string;

  @IsEmail()
  email: string;

  @Length(10, 10)
  national_code: string;

  @IsString()
  @MinLength(8)
  @Matches(/[A-Z]/)
  @IsAlphanumeric()
  password: string;
}