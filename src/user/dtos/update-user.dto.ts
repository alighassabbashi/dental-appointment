import { IsOptional, IsString, IsNumber, IsEmail, Min, Max, IsIn, MinLength, ValidateIf, Matches, IsAlphanumeric, Length } from 'class-validator';

export class updateUserDto {

  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  lastname: string;

  @IsNumber()
  @IsOptional()
  @Min(0)
  @Max(100)
  age: number;

  @IsString()
  @IsOptional()
  @IsIn(['male', 'female'])
  gender: string;

  @IsString()
  @IsOptional()
  @IsIn(['tamin', 'niroo', 'talayi', 'takmili', 'roostayi', 'salamat'])
  insurance_type: string;

  @IsString()
  @IsOptional()
  @MinLength(11)
  phone_number: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @Length(10, 10)
  @IsOptional()
  national_code: string;

  @IsString()
  @IsOptional()
  @MinLength(8)
  @Matches(/[A-Z]/)
  @IsAlphanumeric()
  password: string;
}