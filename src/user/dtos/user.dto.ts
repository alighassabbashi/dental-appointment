import { Expose } from 'class-transformer'

export class UserDto {
  @Expose()
  _id: string;

  @Expose()
  name: string;

  @Expose()
  lastname: string;

  @Expose()
  age: number;

  @Expose()
  gender: string;

  @Expose()
  insurance_type: string;

  @Expose()
  phone_number: string;

  @Expose()
  national_code: string;

  @Expose()
  email: string;
}