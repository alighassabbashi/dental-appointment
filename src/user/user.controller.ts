import { Body, Controller, Post, Get, Patch, Delete, Param, Query, NotFoundException, Session, UseGuards, UnauthorizedException} from '@nestjs/common';
import { CreateUserDto } from './dtos/create-user.dto';
import { updateUserDto } from './dtos/update-user.dto';
import { SigninUserDto } from './dtos/singin-user-dto';

import { UserService } from './user.service';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UserDto } from './dtos/user.dto';
import { Serialize } from '../interceptors/serialize.interceptor';

import { CurrentUser } from './decorators/current-user.decorator';
import { AuthGuard } from '../guards/auth.guard'
import { ChangeRoleDto } from './dtos/change-role.dto';
import { AdminGuard } from '../guards/admin.guard';

@Controller('user')
@Serialize(UserDto)
export class UserController {
  constructor(
    private userService: UserService,
    private authService: AuthService
  ) { }

  @Get('/get-user')
  @UseGuards(AuthGuard)
  getUser(@CurrentUser() user: User) {
    return user;
  }

  @Post('/signup')
  async createUser(@Body() body: CreateUserDto, @Session() session: any) {
    const user = await this.authService.signup(body);
    session.userId = user._id;
    return user;
  }

  @Post('/signin')
  async signUserIn(@Body() body: SigninUserDto, @Session() session: any) {
    const user = await this.authService.signin(body.email, body.password);
    session.userId = user._id;
    return user;
  }

  @Post('/sign-out')
  signOut(@Session() session: any) {
    session.userId = null;
  }

  @Post('/change-role/:id')
  @UseGuards(AdminGuard)
  changeRole(@Param('id') id: string, @Body() body: ChangeRoleDto) {
    return this.authService.changeRole(id, body.adminKey, body.role);
  }

  @Get()
  @UseGuards(AdminGuard)
  getAllUsers() {
    return this.userService.findAll();
  }

  @Get('/search')
  @UseGuards(AdminGuard)
  searchAmongUsers(@Query() payload: Partial<User>) {
    return this.userService.findByPartialInfo(payload)
  }
  
  @Get('/email')
  @UseGuards(AuthGuard)
  async getUserByEmail(@CurrentUser() currentUser: User, @Query('email') email: string) {
    const user = await this.userService.findByEmail(email)
    if (user._id !== currentUser._id) {
      if (currentUser.role !== 'admin') {
        throw new UnauthorizedException('you are not allowd to get this user info!')
      }
    }
    if (!user) {
      throw new NotFoundException('user not found!')
    }
    return user
  }

  @Get('/:id')
  async getUserById(@Param('id') id: string) {
    const user = await this.userService.findById(id)
    if (!user) {
      throw new NotFoundException('user not found!')
    }
    return user
  }

  @Patch('/:id')
  updateUser(@Param('id') id: string, @Body() payload: updateUserDto) {
    return this.userService.updateUser(id, payload);
  }

  @Delete('/:id')
  @UseGuards(AdminGuard)
  deleteUser(@Param('id') id: string) {
    return this.userService.deleteUser(id);
  }
}
