import { Entity, Column, Generated, PrimaryColumn, OneToMany, PrimaryGeneratedColumn} from 'typeorm'

import { Visit } from '../visit/visit.entity';
import { Invoice } from '../invoice/invoice.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  _id: string;

  @Column()
  name: string;

  @Column()
  lastname: string;

  @Column()
  age: number;

  @Column()
  gender: string;

  @Column()
  insurance_type: string;

  @Column()
  phone_number: string;

  @Column({unique: true})
  email: string;

  @Column()
  national_code: string;

  @OneToMany(() => Visit, (visit) => visit.user)
  visits: Visit[];

  @OneToMany(() => Invoice, (invoice) => invoice.user)
  invoices: Invoice[];

  @Column({ default: 'basic' })
  role: string;
  
  @Column()
  password: string;
}