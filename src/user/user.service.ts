import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { CreateUserDto } from './dtos/create-user.dto';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private repo: Repository<User>) { }
  
  create(user: CreateUserDto) {
    // if craeteUserDto was deferent from user structure that we want to save to our database
    // we can create another dto
    // and "create()" argument most be of type of that dto
    const createdUser = this.repo.create(user)
    return this.repo.save(createdUser);
  }

  async findById(id: string) {
    if (!id) {
      return null;
    }
    const user = await this.repo.findOne({where: {_id: id}})
    return user;
  }

  async findByEmail(email: string) {
    const user = await this.repo.findOne({ where: { email } })
    return user;
  }

  async findByPartialInfo(payload: Partial<User>) {
    const users = await this.repo.find({ where: payload })
    return users;
  }

  async findAll() {
    const users = await this.repo.find();
    return users;
  }
  
  async updateUser(id: string, payload: Partial<User>) {
    const user = await this.findById(id);
    if (!user) {
      throw new NotFoundException('user not found!');
    }
    Object.assign(user, payload)
    return this.repo.save(user);
  }

  async updateRole(id: string, role: string) {
    const user = await this.findById(id);
    if (!user) {
      throw new NotFoundException('user not found!');
    }
    user.role = role;
    return await this.repo.save(user);
  }

  async deleteUser(id: string) {
    const user = await this.findById(id);
    if (!user) {
      throw new NotFoundException('user not found!');
    }
    return this.repo.remove(user);
  }
  
}
