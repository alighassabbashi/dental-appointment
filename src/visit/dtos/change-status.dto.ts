import { IsIn, IsString } from "class-validator";

export class ChangeStatusDto {
  @IsString()
  @IsIn(['awaiting-payment', 'paid'])
  status: string;
}