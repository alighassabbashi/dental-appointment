import { IsArray, IsDate, IsIn, IsNumber, IsString, IsUUID } from "class-validator";

import { Service } from "../../service/service.entity";

export class CreateVisitDto {
  @IsString()
  dateTime: string;

  // 'initiated', 'awaiting-payment', 'paid' : statuses

  @IsArray()
  requiredServices: Service[];
}