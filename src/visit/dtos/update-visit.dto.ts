import { IsArray, IsOptional, IsString } from "class-validator";

import { Service } from "../../service/service.entity";

export class UpdateVisitDto {
  @IsString()
  @IsOptional()
  dateTime: string;
  
  @IsArray()
  @IsOptional()
  requiredServices: Service[];
}