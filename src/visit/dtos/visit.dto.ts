import { Expose, Transform } from "class-transformer";
import { Invoice } from "src/invoice/invoice.entity";
import { Service } from "src/service/service.entity";
import { User } from "src/user/user.entity";

export class VisitDto {
  @Expose()
  _id: string;

  @Expose()
  status: string;

  @Expose()
  dateTime: string;

  @Expose()
  duration: number;

  @Transform(({ obj }) => obj.user._id)
  @Expose()
  userId: User;

  @Transform(({ obj }) => obj.requiredServices)
  @Expose()
  requiredServices: Service[];

  @Transform(({obj}) => obj.invoice ? obj.invoice._id:null)
  @Expose()
  invoiceId: Invoice;

  @Expose()
  cost: number;
}