import { Body, Controller, Param, Patch, Post,Delete, Get, UseGuards } from '@nestjs/common';

import { CreateVisitDto } from './dtos/create-visit.dto';
import { VisitService } from './visit.service';

import { CurrentUser } from '../user/decorators/current-user.decorator';
import { User } from '../user/user.entity';
import { AuthGuard } from '../guards/auth.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { VisitDto } from './dtos/visit.dto';
import { AdminGuard } from '../guards/admin.guard';
import { ChangeStatusDto } from './dtos/change-status.dto';
import { UpdateVisitDto } from './dtos/update-visit.dto';
import { serialize } from 'v8';

@Controller('visit')
@Serialize(VisitDto)
export class VisitController {
  constructor(private visitService: VisitService) { }
  
  @Post('/create')
  @UseGuards(AuthGuard)
  async create(@Body() body: CreateVisitDto, @CurrentUser() user: User) {
    return this.visitService.create(body, user);
  }

  @Get('/user-visits')
  @UseGuards(AuthGuard)
  findUserVisits(@CurrentUser() user: User) {
    return this.visitService.findUserVisits(user);
  }

  @Get()
  @UseGuards(AdminGuard)
  findAll() {
    return this.visitService.findAll();
  }

  @Get('/:id')
  @UseGuards(AuthGuard)
  findVisit(@Param('id') id: string, @CurrentUser() currentUser: User) {
    return this.visitService.findVisit(id, currentUser);
  }
  
  @Patch('/change-status/:id')
  @UseGuards(AuthGuard)
  changeStatus(@Param('id') id: string,@Body() body: ChangeStatusDto) {
    return this.visitService.changeStatus(id, body.status);
  }

  @Patch('/:id')
  @UseGuards(AuthGuard)
  updateVisit(@Param('id') id: string, @Body() body: UpdateVisitDto) {
    return this.visitService.update(id, body)
  }

  @Patch('/cancel-visit/:id')
  @UseGuards(AuthGuard)
  cancelVisit(@Param('id') id: string, @CurrentUser() currentUser: User) {
    return this.visitService.cancel(id, currentUser);
    // why it has dedicated route :
    // --> canceling a visit maybe has a reason or maybe has a compensation
  }

  @Delete('/:id')
  @UseGuards(AdminGuard)
  async delete(@Param('id') id: string) {
    this.visitService.delete(id);
  }
}
