import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany, JoinTable, OneToOne, JoinColumn } from 'typeorm'

import { User } from '../user/user.entity';
import { UnregisteredUser } from '../unregistered-user/unregistered-user.entity';
import { Service } from '../service/service.entity';
import { Invoice } from 'src/invoice/invoice.entity';

@Entity()
export class Visit {
  @PrimaryGeneratedColumn()
  _id: string;

  @Column({ default: 'initiated'})
  status: string;

  @Column()
  dateTime: string;

  @Column()
  duration: number;

  @ManyToOne(() => User, (user) => user.visits)
  user: User;

  @ManyToMany(() => Service)
  @JoinTable()
  requiredServices: Service[];

  @ManyToOne(() => UnregisteredUser, (UnregisteredUser) => UnregisteredUser.visits)
  unregistered_user: UnregisteredUser;

  @OneToOne(() => Invoice)
  @JoinColumn()
  invoice: Invoice;

  @Column()
  cost: number;
}