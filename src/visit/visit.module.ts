import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { VisitController } from './visit.controller';
import { VisitService } from './visit.service';
import { Visit } from './visit.entity'
import { ServiceService } from '../service/service.service';
import { ServiceModule } from '../service/service.module';

@Module({
  imports: [TypeOrmModule.forFeature([Visit]), ServiceModule],
  controllers: [VisitController],
  providers: [VisitService],
  exports: [VisitService]
})
export class VisitModule {}
