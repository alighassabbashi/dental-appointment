import { Injectable, NotFoundException, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CreateVisitDto } from './dtos/create-visit.dto';
import { Visit } from './visit.entity';
import { User } from '../user/user.entity';
import { ServiceService } from '../service/service.service';

@Injectable()
export class VisitService {
  constructor(private serviceService: ServiceService, @InjectRepository(Visit) private repo: Repository<Visit>) { }
  
  async create(body: CreateVisitDto, user: User) {
    const createdVisit = this.repo.create(body);
    createdVisit.cost = 0;
    createdVisit.requiredServices = [];
    createdVisit.duration = 0;
    for (let s in body.requiredServices) {
      const service = await this.serviceService.findById(body.requiredServices[s].toString())
      createdVisit.requiredServices.push(service);
      createdVisit.cost += service.cost;
      createdVisit.duration += service.duration;
    }
    createdVisit.invoice = null;
    createdVisit.user = user;
    return this.repo.save(createdVisit);
  }

  async findById(id: string) {
    const visit = await this.repo.findOne({ where: { _id: id },relations: {
        requiredServices: true,
        user: true,
        invoice: true
      } })
    if (!visit) {
      throw new NotFoundException('visit not found!');
    }
    return visit
  }

  async findVisit(id: string, currentUser: User) {
    const visit = await this.findById(id);
    if (currentUser._id !== visit.user._id) {
      throw new UnauthorizedException('not allowed to get this visit info!')
    }
    return visit;
  }

  async findUserVisits(user: User) {
    const visits = await this.repo.find({
      where: { user }, relations: {
        requiredServices: true,
        user: true
      }
    })
    return visits
  }

  async findAll() {
    return this.repo.find({
      relations: {
        requiredServices: true,
        user: true
      }
    });
  }

  async changeStatus(id: string, status: string) {
    const visit = await this.findById(id);
    console.log(visit, status);
    visit.status = status;
    return await this.repo.save(visit);
  }

  async update(id: string, payload: Partial<Visit>) {
    const visit = await this.findById(id);
    visit.dateTime = payload.dateTime || visit.dateTime;
    if (payload.requiredServices) {
      if (visit.status === 'paid') {
        throw new BadRequestException('can\'t change required services when visit is paied')
      }
      visit.duration = 0;
      visit.cost = 0;
      visit.requiredServices = []
      for (let s in payload.requiredServices) {
        const service = await this.serviceService.findById(payload.requiredServices[s].toString());
        visit.requiredServices.push(service);
        
        visit.cost += service.cost;
        visit.duration += service.duration;
      }
    }
    if (payload.invoice) {
      visit.invoice = payload.invoice;
    }
    return await this.repo.save(visit);
  }

  async cancel(id: string, currentUser: User) {
    const visit = await this.findById(id);
    if (currentUser._id !== visit.user._id) {
      if (currentUser.role !== 'admin') {
        throw new UnauthorizedException('you are not allowd to cancel this visit');
      }
    }
    visit.status = 'canceled';
    return this.repo.save(visit);
  }

  async delete(id: string) {
    const visit = await this.findById(id);
    return this.repo.remove(visit);
  }
}
